﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using SomethingWebApi.Models;
using System.Threading.Tasks;

namespace TodoApi.Controllers
{
    //[Route("api/[controller]")]
    [Route("something")]
    [ApiController]
    public class SomethingContorller : ControllerBase
    {
        private readonly SomethingContext _context;
        public SomethingContorller(SomethingContext context)
        {
            _context = context;
            // if (_context.Somethings.Count() == 0)
            // {
            //     _context.Somethings.Add(new Something { Name = "Item1" }); _context.SaveChanges();
            // }
        }

        [HttpGet]
        public ActionResult<List<Something>> GetAll()
        {
            return _context.Somethings.ToList();
        }

        [HttpGet("{id}", Name = "something")]
        public async Task<ActionResult<Something>> GetById(int id)
        { 

            var item = await _context.Somethings.FindAsync(id);
            
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public async Task<ActionResult<Something>> PostSomething(Something something)
        {
            _context.Somethings.Add(something);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(Something), new { id = something.Id }, something);
        }
    }
}