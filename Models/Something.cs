
using System.ComponentModel.DataAnnotations.Schema;

namespace SomethingWebApi.Models {     

    [Table("something")]
    public class Something  
    {
        
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; }         
    }
}