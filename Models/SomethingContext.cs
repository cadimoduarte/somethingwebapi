using Microsoft.EntityFrameworkCore;

namespace SomethingWebApi.Models

{
    public class SomethingContext : DbContext
    {
        public SomethingContext(DbContextOptions<SomethingContext> options)  : base(options) {}
        public DbSet<Something> Somethings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            modelbuilder.HasSequence<int>("something_seq").StartsAt(1);

            modelbuilder.Entity<Something>()
            .Property(o => o.Id)
            .HasDefaultValueSql<int>("select nextval('something_seq')");
            //.HasDefaultValueSql("NEXT VALUE FOR shared.OrderNumbers");

        }

    }
}